package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CodeWriter {
	private FileWriter writer;
	private String currentFileName;

	public CodeWriter(File asmDest) {
		try {
			writer = new FileWriter(asmDest);
		}
		catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void setFileName(String fileName) {
		currentFileName = fileName;
	}

	public void writeInit() {
		write("@256"); //set SP to 256
		write("D=A");
		write("@SP");
		write("M=D");

		//		write("@101"); //no need to preset this registers. Only for better debugging
		//		write("D=A");
		//		write("@LCL");
		//		write("M=D");
		//
		//		write("@102");
		//		write("D=A");
		//		write("@ARG");
		//		write("M=D");
		//
		//		write("@103");
		//		write("D=A");
		//		write("@THIS");
		//		write("M=D");
		//
		//		write("@104");
		//		write("D=A");
		//		write("@THAT");
		//		write("M=D");

		writeCall("Sys.init", 0);
	}

	private int arithJump = 0;

	public void writeArithmetic(String command) {
		arithJump++; //used for the jumps to not have jumps used twice in asm

		if (command.equals("neg") || command.equals("not")) { //unary commands
			write("@SP");
			write("A=M-1"); //decrease stack pointer value by 1 and save in A reg not in sp
			write(command.equals("not") ? "M=!M" : "M=-M"); //take RAM[A] and save new value there
			//no ned to inc sp because not decreased physically
			return;
		}

		//the value sp-1 should go in D reg, the value at sp-2 should go in A reg, thus we have to compute A-D not D-A if the order matters
		write("@SP");
		write("AM=M-1"); //decrease stack pointer value by 1 and save also in A reg
		write("D=M"); //take the RAM value at sp - 1 (from A reg)

		write("@SP");
		write("AM=M-1");
		write("A=M"); //same as above, but the second value relies in A now

		switch (command) {
			case "add":
				write("D=D+A");
				break;
			case "sub":
				write("D=A-D");
				break;
			case "and":
				write("D=D&A");
				break;
			case "or":
				write("D=D|A");
				break;
			case "eq":
				write("D=A-D");
				write("@JOK" + arithJump);
				write("D;JEQ"); //if 0 -> equality
				write("D=0"); //unequal
				write("@JEND" + arithJump);
				write("0;JMP");
				write("(JOK" + arithJump + ")");
				write("D=-1");
				write("(JEND" + arithJump + ")");
				break;
			case "gt":
				write("D=A-D");
				write("@JOK" + arithJump);
				write("D;JGT");
				write("D=0");
				write("@JEND" + arithJump);
				write("0;JMP");
				write("(JOK" + arithJump + ")");
				write("D=-1");
				write("(JEND" + arithJump + ")");
				break;
			case "lt":
				write("D=A-D");
				write("@JOK" + arithJump);
				write("D;JLT");
				write("D=0");
				write("@JEND" + arithJump);
				write("0;JMP");
				write("(JOK" + arithJump + ")");
				write("D=-1");
				write("(JEND" + arithJump + ")");
				break;
			default:
				break;
		}

		write("@SP");
		write("A=M"); //the content of the stack pointer
		write("M=D"); //save the result of the arithmetic command in the place sp - 2

		write("@SP"); //increase stack pointer by 1 again
		write("M=M+1");
	}

	public void writePushPop(String pushPop, String segment, int index) {
		if (pushPop.equals("C_PUSH")) {
			if (segment.equals("constant")) { //add a number to the stack
				write("@" + index);
				write("D=A");
			}
			else if (segment.equals("static")) {
				write("@" + currentFileName + "." + index); //like: @StaticTest.3    for push static 3
				write("D=M");
			}
			else {
				if (segment.equals("local")) {
					write("@LCL");
					write("D=M");
				}
				else if (segment.equals("argument")) {
					write("@ARG");
					write("D=M");
				}
				else if (segment.equals("this")) {
					write("@THIS");
					write("D=M");
				}
				else if (segment.equals("that")) {
					write("@THAT");
					write("D=M");
				}
				else if (segment.equals("pointer")) {
					write("@3");
					write("D=A");
				}
				else if (segment.equals("temp")) {
					write("@5");
					write("D=A");
				}
				write("@" + index); //add the index to the base address like RAM[pointer + 1] is the same as RAM[that]
				write("A=D+A"); //the RAM pos of the value
				write("D=M"); //save the value in the D reg
			}

			write("@SP"); //the value which have to be stored should here already be in the D Register
			write("A=M");
			write("M=D"); //write data

			write("@SP"); //increase stack pointer by 1
			write("M=M+1");
		}
		else if (pushPop.equals("C_POP")) {
			if (segment.equals("static")) {
				write("@SP");
				write("AM=M-1"); //decrease SP and save value also in A reg
				write("D=M"); //value which should be popped
				write("@" + currentFileName + "." + index); //like: @StaticTest.3    for push static 3
				write("M=D");
				return;
			}

			if (segment.equals("local")) {
				write("@LCL");
				write("D=M");
			}
			else if (segment.equals("argument")) {
				write("@ARG");
				write("D=M");
			}
			else if (segment.equals("this")) {
				write("@THIS");
				write("D=M");
			}
			else if (segment.equals("that")) {
				write("@THAT");
				write("D=M");
			}
			else if (segment.equals("pointer")) {
				write("@3");
				write("D=A");
			}
			else if (segment.equals("temp")) {
				write("@5");
				write("D=A");
			}

			write("@" + index);
			write("D=D+A"); //destination address
			write("@R15"); //save in R15
			write("M=D");

			write("@SP");
			write("AM=M-1"); //decrease SP and save value also in A reg
			write("D=M"); //value which should be popped

			write("@R15"); //get the destination back
			write("A=M");
			write("M=D"); //save the value
		}
	}

	/**
	 * 
	 * @param fct$label
	 *            already parsed in function$label
	 */
	public void writeLabel(String fct$label) {
		write("(" + fct$label + ")");
	}

	/**
	 * Unconditional jump.
	 * 
	 * @param fct$label
	 *            already parsed in function$label
	 */
	public void writeGoto(String fct$label) {
		write("@" + fct$label);
		write("0;JMP");
	}

	/**
	 * Conditional jump. Always jumps, expect for value 0. If 0, the next
	 * command is executed. The value inspected is the topmost element of the
	 * stack
	 * 
	 * @param fct$label
	 *            already parsed in function$label
	 */
	public void writeIf(String fct$label) {
		write("@SP");
		write("AM=M-1"); //decrease SP and save value also in A reg
		write("D=M"); //value which should be popped
		write("@" + fct$label);
		write("D;JNE");
	}

	private int call = 0; //to get unique id for each function call (especially for more calls in one file which are calling the same function)

	public void writeCall(String functionName, int numArgs) {
		call++;

		write("@" + functionName + ".RETURN_" + call); //push return-address
		write("D=A");
		write("@SP");
		write("A=M");
		write("M=D");
		write("@SP");
		write("M=M+1");

		write("@LCL"); //push current LCL of the calling function (location to which it is pointing)
		write("D=M");
		write("@SP");
		write("A=M");
		write("M=D");
		write("@SP");
		write("M=M+1");

		write("@ARG"); //push current ARG of the calling function (location to which it is pointing)
		write("D=M");
		write("@SP");
		write("A=M");
		write("M=D");
		write("@SP");
		write("M=M+1");

		write("@THIS"); //push current THIS of the calling function (location to which it is pointing)
		write("D=M");
		write("@SP");
		write("A=M");
		write("M=D");
		write("@SP");
		write("M=M+1");

		write("@THAT"); //push current THAT of the calling function (location to which it is pointing)
		write("D=M");
		write("@SP");
		write("A=M");
		write("M=D");
		write("@SP");
		write("M=M+1");

		//reposition ARG
		int argPosBeforeSP = numArgs + 5; //the new arg base should point there; see page 162f
		write("@SP");
		write("D=M");
		write("@" + argPosBeforeSP);
		write("D=D-A");
		write("@ARG");
		write("M=D");

		//reposition LCL
		write("@SP");
		write("D=M");
		write("@LCL");
		write("M=D");

		//goto function
		write("@" + functionName);
		write("0;JMP");

		//here comes the return-address to which is jumped after the called function returns
		write("(" + functionName + ".RETURN_" + call + ")");
	}

	public void writeFunction(String functionName, int numLocals) {
		write("(" + functionName + ")");

		for (int i = 0; i < numLocals; i++) {
			write("@0"); //same as: push constant 0
			write("D=A");
			write("@SP"); //the value which have to be stored should here already be in the D Register
			write("A=M");
			write("M=D"); //write data

			write("@SP"); //increase stack pointer by 1
			write("M=M+1");
		}
	}

	public void writeReturn() {
		//save LCL in R13, take R13 for calculations
		write("@LCL");
		write("D=M");
		write("@R13");
		write("M=D");

		//save the return address in R14, R14 = R13 - 5
		write("@R13");
		write("D=M");
		write("@5");
		write("A=D-A"); //take the address which is stored in the LCL-5 location
		write("D=M");
		write("@R14");
		write("M=D");

		//set the return value of the called function at the place of argument 0 (pop it from the current topmost position on the stack)
		writePushPop("C_POP", "argument", 0);

		//set SP at ARG + 1 (one ahead of the return value)
		write("@ARG");
		write("D=M+1");
		write("@SP");
		write("M=D");

		//restore THAT of the caller: THAT = R13 - 1
		write("@R13");
		write("A=M-1");
		write("D=M");
		write("@THAT");
		write("M=D");

		//restore THIS of the caller: THIS = R13 - 2
		write("@R13");
		write("D=M");
		write("@2");
		write("A=D-A");
		write("D=M");
		write("@THIS");
		write("M=D");

		//restore ARG of the caller: ARG = R13 - 3
		write("@R13");
		write("D=M");
		write("@3");
		write("A=D-A");
		write("D=M");
		write("@ARG");
		write("M=D");

		//restore LCL of the caller: LCL = R13 - 4
		write("@R13");
		write("D=M");
		write("@4");
		write("A=D-A");
		write("D=M");
		write("@LCL");
		write("M=D");

		//goto return-address of the caller, R14
		write("@R14");
		write("A=M");
		write("0;JMP");
	}

	private void write(String toWrite) {
		try {
			writer.write(toWrite + "\n");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
