package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StartTranslator {
	/**
	 * Can either be a directory or a single .vm file.
	 */
	public static File toTranslate = new File("D:/nand2tetris/projects/08/FunctionCalls/StaticsTest");
	private static List<File> filesToTranslate = new ArrayList<File>();

	public static void main(String[] args) {
		if (toTranslate.isDirectory()) {
			for (File f : toTranslate.listFiles()) {
				if (f.getAbsolutePath().contains(".vm")) {
					filesToTranslate.add(f);
				}
			}
		}
		else if (toTranslate.isFile()) {
			if (toTranslate.getAbsolutePath().contains(".vm")) {
				filesToTranslate.add(toTranslate);
			}
		}
		if (filesToTranslate.size() == 0) {
			System.out.println("No files selected!");
			System.exit(0);
		}

		File destinationASMFile = getDestFile();
		destinationASMFile.delete();
		try {
			destinationASMFile.createNewFile();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		CodeWriter writer = new CodeWriter(destinationASMFile);

		for (File currentTranslation : filesToTranslate) {
			if (currentTranslation.getAbsolutePath().contains("Sys.vm")) {
				writer.writeInit();
				break;
			}
		}

		for (File currentTranslation : filesToTranslate) {
			int lastIndex = currentTranslation.getAbsolutePath().lastIndexOf("\\"); //extract the file name
			String fileName = currentTranslation.getAbsolutePath().substring(lastIndex + 1).replace(".vm", "");
			writer.setFileName(fileName);
			System.out.println("Translating file: " + fileName);

			String currentFunction = "null"; //if no function was explicitly called: default is null

			Parser parser = new Parser(currentTranslation);
			while (parser.hasMoreCommands()) {
				parser.advance();
				String command = parser.commandType();
				if (command == null) {
					continue;
				}

				if (command.equals("C_ARITHMETIC")) {
					writer.writeArithmetic(parser.arg1());
				}
				else if (command.equals("C_PUSH") || command.equals("C_POP")) {
					writer.writePushPop(command, parser.arg1(), parser.arg2());
				}
				else if (command.equals("C_LABEL")) {
					writer.writeLabel(currentFunction + "$" + parser.arg1());
				}
				else if (command.equals("C_GOTO")) {
					writer.writeGoto(currentFunction + "$" + parser.arg1());
				}
				else if (command.equals("C_IF")) {
					writer.writeIf(currentFunction + "$" + parser.arg1());
				}
				else if (command.equals("C_CALL")) {
					writer.writeCall(parser.arg1(), parser.arg2());
				}
				else if (command.equals("C_FUNCTION")) {
					currentFunction = parser.arg1();
					writer.writeFunction(currentFunction, parser.arg2());
				}
				else if (command.equals("C_RETURN")) {
					writer.writeReturn();
				}
			}
			parser.close();
			System.out.println("finished");
		}
		writer.close();
	}

	private static File getDestFile() {
		if (toTranslate.isDirectory()) {
			int lastSlash = toTranslate.getAbsolutePath().lastIndexOf("\\");
			return new File(toTranslate.getAbsolutePath() + toTranslate.getAbsolutePath().substring(lastSlash) + ".asm");
		}
		else if (toTranslate.isFile()) {
			return new File(toTranslate.getAbsolutePath().replace(".vm", ".asm"));
		}
		return null;
	}
}
