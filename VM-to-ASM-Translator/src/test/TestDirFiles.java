package test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestDirFiles {
	public static File toTranslate = new File("D:/nand2tetris/projects/08/FunctionCalls/FibonacciElement");
	private static List<File> filesToTranslate = new ArrayList<File>();

	@Test
	public void test() {
		if (toTranslate.isDirectory()) {
			for (File f : toTranslate.listFiles()) {
				if (f.getAbsolutePath().contains(".vm")) {
					filesToTranslate.add(f);
				}
			}
		}
		else if (toTranslate.isFile()) {
			if (toTranslate.getAbsolutePath().contains(".vm")) {
				filesToTranslate.add(toTranslate);
			}
		}
		if (filesToTranslate.size() == 0) {
			System.out.println("No files selected!");
			System.exit(0);
		}
		for (File f : filesToTranslate) {
			System.out.println(f.getAbsolutePath());
		}

		System.out.println("\nDestination: ");
		if (toTranslate.isDirectory()) {
			int lastSlash = toTranslate.getAbsolutePath().lastIndexOf("\\");
			System.out.println(toTranslate.getAbsolutePath() + toTranslate.getAbsolutePath().substring(lastSlash) + ".asm");
		}
		else if (toTranslate.isFile()) {
			System.out.println(toTranslate.getAbsolutePath().replace(".vm", ".asm"));
		}
	}
}
