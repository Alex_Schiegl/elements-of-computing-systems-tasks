package main;

import java.io.File;
import java.io.IOException;

public class StartTranslator {
	//	public static File toTranslate = new File("D:/nand2tetris/projects/07/StackArithmetic/SimpleAdd/SimpleAdd.vm"); //instead of args-param
	//	public static File toTranslate = new File("D:/nand2tetris/projects/07/StackArithmetic/StackTest/StackTest.vm"); //instead of args-param
	//	public static File toTranslate = new File("D:/nand2tetris/projects/07/MemoryAccess/BasicTest/BasicTest.vm"); //instead of args-param
	//	public static File toTranslate = new File("D:/nand2tetris/projects/07/MemoryAccess/PointerTest/PointerTest.vm"); //instead of args-param
	public static File toTranslate = new File("D:/nand2tetris/projects/07/MemoryAccess/StaticTest/StaticTest.vm"); //instead of args-param

	public static void main(String[] args) {
		Parser parser = new Parser(toTranslate);
		File destinationASMFile = new File(toTranslate.getAbsolutePath().replace(".vm", ".asm"));
		destinationASMFile.delete();
		try {
			destinationASMFile.createNewFile();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		CodeWriter writer = new CodeWriter(destinationASMFile);

		int lastIndex = toTranslate.getAbsolutePath().lastIndexOf("\\");
		String fileName = toTranslate.getAbsolutePath().substring(lastIndex + 1).replace(".vm", "");
		writer.setFileName(fileName);
		System.out.println("Translating file: " + fileName);

		while (parser.hasMoreCommands()) {
			parser.advance();
			String command = parser.commandType();
			if (command == null) {
				continue;
			}

			if (command.equals("C_ARITHMETIC")) {
				writer.writeArithmetic(parser.arg1());
			}
			else if (command.equals("C_PUSH") || command.equals("C_POP")) {
				writer.writePushPop(command, parser.arg1(), parser.arg2());
			}
		}

		parser.close();
		writer.close();
		System.out.println("finished");
	}
}
