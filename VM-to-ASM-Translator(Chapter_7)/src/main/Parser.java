package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Parser {
	private Scanner c;
	private String currentLine;

	public Parser(File vmFile) {
		try {
			c = new Scanner(new FileInputStream(vmFile));
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public boolean hasMoreCommands() {
		return c.hasNextLine();
	}

	public void advance() {
		currentLine = c.nextLine().trim();
		if (currentLine.contains("//"))
			currentLine = currentLine.substring(0, currentLine.indexOf("//")).trim();
	}

	public String commandType() {
		if (getArithmeticCommand() != null) {
			return "C_ARITHMETIC";
		}
		if (currentLine.contains("push")) {
			return "C_PUSH";
		}
		if (currentLine.contains("pop")) {
			return "C_POP";
		}
		if (currentLine.contains("label")) {
			return "C_LABEL";
		}
		if (currentLine.contains("goto")) {
			return "C_GOTO";
		}
		if (currentLine.contains("if")) {
			return "C_if";
		}
		if (currentLine.contains("function")) {
			return "C_FUNCTION";
		}
		if (currentLine.contains("return")) {
			return "C_RETURN";
		}
		if (currentLine.contains("call")) {
			return "C_CALL";
		}
		return null;
	}

	public String arg1() {
		String arithC = getArithmeticCommand();
		if (arithC != null) {
			return arithC;
		}
		return currentLine.split(" ")[1]; //returns from line "push constant 12" -> constant
	}

	public int arg2() {
		String arg = currentLine.split(" ")[2];
		return new Integer(arg);
	}

	/**
	 * 
	 * @return The arithmetic command as string, if no arithmetic command: null
	 */
	private String getArithmeticCommand() {
		if (currentLine.equals("add") || currentLine.equals("sub") || currentLine.equals("neg") || currentLine.equals("eq") || currentLine.equals("gt") || currentLine.equals("lt") || currentLine.equals("and") || currentLine.equals("or") || currentLine.equals("not")) {
			return currentLine;
		}
		return null;
	}

	public void close() {
		c.close();
	}
}
