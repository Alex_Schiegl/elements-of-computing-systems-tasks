package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import symbol.SymbolTable;

/**
 * Hack platform assembler. Proposed implementation of the book
 * "The elements of computing systems" used.
 * 
 * @author Alex
 *
 */
public class StartAssembler {
	private static File source;
	private static File destination;
	private static File noL; //file with no l_commands -- only without the () ROM files
	private static File noS; //file with no symbols
	private static SymbolTable st = new SymbolTable();

	public static void main(String[] args) throws IOException {
		source = new File("D:/nand2tetris/projects/06/" + args[0] + "/" + args[0] + ".asm");
		noL = new File(source.getAbsolutePath().replace(".asm", ".noL"));
		noS = new File(source.getAbsolutePath().replace(".asm", ".noS"));
		destination = new File(source.getAbsolutePath().replace(".asm", "_self.hack"));

		removeJumpDestinations();
		removeSymbols();
		assemble();
	}

	/**
	 * First step of executing
	 */
	private static void removeJumpDestinations() throws IOException {
		int lineCounter = -1;

		noL.delete();
		noL.createNewFile();
		//		middle.deleteOnExit();
		Parser sourceParser = new Parser(source);
		FileWriter writeNoL = new FileWriter(noL);

		while (sourceParser.hasMoreCommands()) { //l_commands parsing ()
			sourceParser.advance();
			String toPrint = sourceParser.getCurrentLine();

			String commandType = sourceParser.commandType();
			if (commandType == null) {
				continue;
			}

			lineCounter++;
			String symbol = sourceParser.symbol();

			if (commandType.equals("l_command")) { //ROM
				if (!st.contains(symbol)) {
					st.addEntry(symbol, lineCounter);
				}
				toPrint = "";
				lineCounter--;
			}

			writeNoL.append(toPrint + "\n");
		}
		writeNoL.close();
	}

	/**
	 * Second step of executing
	 */
	private static void removeSymbols() throws IOException {
		Parser noLParser = new Parser(noL);
		FileWriter writeNoS = new FileWriter(noS);
		while (noLParser.hasMoreCommands()) { //l_commands parsing ()
			noLParser.advance();
			String toPrint = noLParser.getCurrentLine();

			String commandType = noLParser.commandType();
			if (commandType == null) {
				continue;
			}
			String symbol = noLParser.symbol();

			try {
				new Integer(symbol);
				writeNoS.append(toPrint + "\n");
				continue; //numbers should not be mapped
			}
			catch (Exception e) {

			}

			if (commandType.equals("a_command")) { //RAM
				if (!st.contains(symbol)) {
					st.addEntry(symbol, st.getNextRAMpos());
					st.incNextRAMpos();
				}

				toPrint = "@" + st.getAddress(symbol);
			}
			writeNoS.append(toPrint + "\n");
		}
		writeNoS.flush();
		writeNoS.close();
	}

	/**
	 * Third step of executing
	 */
	private static void assemble() throws IOException {
		Parser noSParser = new Parser(noS);

		destination.delete();
		destination.createNewFile();
		FileWriter fw = new FileWriter(destination);

		while (noSParser.hasMoreCommands()) {
			noSParser.advance();
			String instr = noSParser.commandType();
			if (instr == null) {
				continue;
			}
			if (instr.equals("a_command")) {
				String acomm = parseDecimalToBinary16Bit(noSParser.symbol());
				System.out.printf("%-6s -> %16s\n", noSParser.getCurrentLine(), acomm);
				fw.write(acomm + "\n");
			}
			else if (instr.equals("c_command")) {
				String comm = "111" + Code.comp(noSParser.comp()) + Code.dest(noSParser.dest()) + Code.jump(noSParser.jump());
				System.out.printf("%-6s -> %16s\n", noSParser.getCurrentLine(), comm);
				fw.write(comm + "\n");
			}
		}

		fw.flush();
		fw.close();
	}

	public static String parseDecimalToBinary16Bit(String toParse) {
		String res = "";
		try {
			int p = new Integer(toParse);
			while (p > 0) {
				res = (p % 2) + res;
				p /= 2;
			}

			//fill up to 16 bit
			while (res.length() < 16) {
				res = "0" + res;
			}
			return res;
		}
		catch (Exception e) {
			return "";
		}
	}
}
