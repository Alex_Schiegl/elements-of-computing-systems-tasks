package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Parser {
	private Scanner c;
	private String currentLine;

	public Parser(File asmFile) {
		try {
			FileInputStream fis = new FileInputStream(asmFile);
			c = new Scanner(fis);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public boolean hasMoreCommands() {
		return c.hasNextLine();
	}

	public void advance() {
		currentLine = c.nextLine().trim();
		if (currentLine.contains("//"))
			currentLine = currentLine.substring(0, currentLine.indexOf("//")).trim();
	}

	public String commandType() {
		if (currentLine == null || currentLine.startsWith("//")) {
			return null;
		}
		if (currentLine.startsWith("@")) {
			return "a_command";
		}
		if (currentLine.contains("=") || currentLine.contains(";")) {
			return "c_command";
		}
		if (currentLine.contains("(") && currentLine.contains(")")) {
			return "l_command";
		}
		return null;
	}

	/**
	 * 
	 * @return @8 -> 8 ; (126) -> 126 ; @a -> a ; (b) -> b
	 */
	public String symbol() {
		return currentLine.replace("@", "").replace("(", "").replace(")", "");
	}

	/**
	 * 
	 * @return ADM=D-A -> ADM
	 */
	public String dest() {
		if (currentLine.contains("=")) {
			return currentLine.substring(0, currentLine.indexOf("="));
		}
		return null;
	}

	/**
	 * 
	 * @return ADM=D-A -> D-A ; 0;JMP -> 0
	 */
	public String comp() {
		if (currentLine.contains(";")) {
			return currentLine.substring(0, currentLine.indexOf(";"));
		}
		if (currentLine.contains("=")) {
			return currentLine.substring(currentLine.indexOf("=") + 1, currentLine.length());
		}
		return null;
	}

	/**
	 * 
	 * @return 0;JMP -> JMP
	 */
	public String jump() {
		if (currentLine.contains(";")) {
			return currentLine.substring(currentLine.indexOf(";") + 1, currentLine.length());
		}
		return null;
	}

	public void printAll() {
		while (c.hasNextLine()) {
			System.out.println(c.nextLine());
		}
	}

	public String getCurrentLine() {
		return currentLine;
	}
}
