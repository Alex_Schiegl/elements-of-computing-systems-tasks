package symbol;

import java.util.TreeMap;

public class SymbolTable {
	private TreeMap<String, Integer> symbolTable;
	private int nextRAMpos = 16;

	public SymbolTable() {
		symbolTable = new TreeMap<String, Integer>();
		symbolTable.put("SP", 0);
		symbolTable.put("LCL", 1);
		symbolTable.put("ARG", 2);
		symbolTable.put("THIS", 3);
		symbolTable.put("THAT", 4);
		symbolTable.put("SCREEN", 16384);
		symbolTable.put("KBD", 24576);

		for (int i = 0; i < 16; i++) {
			symbolTable.put("R" + i, i); //for R0 - R15 addresses
		}
	}

	public int getNextRAMpos() {
		return nextRAMpos;
	}

	public void incNextRAMpos() {
		nextRAMpos++;
	}

	public void addEntry(String symbol, int address) {
		symbolTable.put(symbol, address);
	}

	public boolean contains(String symbol) {
		return symbolTable.containsKey(symbol);
	}

	public int getAddress(String symbol) {
		return symbolTable.get(symbol);
	}
}
